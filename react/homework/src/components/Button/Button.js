import React from 'react';
import './Button.scss'
import PropTypes from 'prop-types'

const Button = ({text, backgroundColor, onClick}) => {

    return (
        <div>
            <button className='button' style={{backgroundColor: backgroundColor}} onClick={onClick}>{text}</button>
        </div>
    );

}

Button.propTypes = {
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func,
}

Button.defaultProps = {
    text: '',
    backgroundColor: ''
}

export default Button;