import Button from "./Button";
import {render} from "@testing-library/react";

describe('Button test', () => {
    test('smoke modal test', () => {
        render(<Button />)
    })

    test('smoke Button test div', () => {
        const {container} = render(<Button/>)
        console.log(container.innerHTML)
    })

    test('snapshot', () => {
        const {firstChild} = render(<Button/>)
        expect(firstChild).toMatchSnapshot()
    })

    test('test render', () => {
        const {container} = render(<Button />)
        expect(container.querySelector('.button')).toBeTruthy()
    })

})