import React from 'react';
import './Icons.scss'
import PropTypes from 'prop-types'
import {useSelector} from "react-redux";

const Icons = ({clickStar, vendorCode, deleteFromSelected}) => {
    const selectedGoodsState = useSelector(state => state.selectedGoods)

    return (
        <>
            {selectedGoodsState?.data.includes(vendorCode) &&
            <i className="fas fa-star star-icon" onClick={deleteFromSelected}/>
            }
            {!selectedGoodsState?.data.includes(vendorCode) &&
            <i className="far fa-star star-icon" onClick={clickStar}/>
            }
        </>
    )

}

Icons.propTypes = {
    clickStar: PropTypes.func,
    isClickedStar: PropTypes.bool,
    deleteFromSelected: PropTypes.func,
}

export default Icons;