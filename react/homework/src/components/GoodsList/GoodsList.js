import React from 'react';
import './GoodsList.scss'
import Goods from "../Goods/Goods";
import PropTypes from 'prop-types'

const GoodsList = ({goods, onClick, clickStarFunc}) => {

    const goodsList = goods?.map((item, key) => <Goods
        key={key}
        name={item.name}
        price={item.price}
        img={item.source}
        vendorCode={item.vendorCode}
        color={item.color}
        onClick={() => onClick(item.vendorCode)}
        clickStarFunc={() => clickStarFunc(item.vendorCode)}
        closeBtn={false}
        isShowedButtons={true}
    />)

    return (
        <ul className='goods-list'>
            {goodsList}
        </ul>
    );

}

GoodsList.propTypes = {
    goods: PropTypes.array,
    onClick: PropTypes.func,
    clickStarFunc: PropTypes.func,
    deleteFromSelectedFunc: PropTypes.func,
    selectedGoods: PropTypes.array,
}

GoodsList.defaultProps = {
    goods: [],
    selectedGoods: [],
}

export default GoodsList;