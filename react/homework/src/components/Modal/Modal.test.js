import Modal from './Modal'
import {render} from '@testing-library/react'

describe('Modal test', () => {
    test('smoke modal test', () => {
        render(<Modal />)
    })

    test('smoke modal test div', () => {
        const {container} = render(<Modal/>)
        console.log(container.innerHTML)
    })

    test('snapshot', () => {
        const {firstChild} = render(<Modal/>)
        expect(firstChild).toMatchSnapshot()
    })

    test('test render', () => {
        const {container} = render(<Modal />)
        expect(container.querySelector('.modal-container')).toBeTruthy()
    })
})

