import * as GOODS_IN_CART from "../constans/goodsInCart";

export function setGoodsInCart(data){
    return function (dispatch){
        return dispatch({
            type: GOODS_IN_CART.GOODS_IN_CART_SET,
            data,
        })
    }
}

