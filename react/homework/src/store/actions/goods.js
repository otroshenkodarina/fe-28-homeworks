import * as GOODS from '../constans/goods';
import axios from "axios";

export function setGoods() {
    return function (dispatch) {
        axios('/goods.json')
            .then(res => {
                if (!res?.data?.length) return
                return dispatch({
                    type: GOODS.GOODS_SET,
                    data: res.data
                })
            })
    }
}

