import * as SELECTED_GOODS from "../constans/selectedGoods";

export function setSelectedGoods(data){
    return function (dispatch){
        return dispatch({
            type: SELECTED_GOODS.SELECTED_GOODS,
            data,
        })
    }
}
