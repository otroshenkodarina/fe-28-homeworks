import * as ADD_TO_CART_ID from '../constans/addToCartId';

export function setCartId(data){
    return function (dispatch){
        return dispatch({
            type: ADD_TO_CART_ID.ADD_TO_CART_ID_SET,
            data,
        })
    }
}