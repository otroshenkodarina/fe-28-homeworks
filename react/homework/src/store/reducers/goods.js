import * as GOODS from '../constans/goods';

const initialState = {
    data: [],
    isLoading: true,
    error: null
}

export default function Goods(state = initialState, action) {
    switch (action.type) {
        case GOODS.GOODS_SET: {
            return {
                ...state,
                data: action.data,
                isLoading: false
            }
        }

        default:
            return state
    }
}

