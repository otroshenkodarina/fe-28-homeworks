import * as ADD_TO_CART_ID from '../constans/addToCartId';

const initialState = {
    data: null,
    isLoading: true,
    error: null
}

export default function AddToCartId(state = initialState, action) {
    switch (action.type) {
        case ADD_TO_CART_ID.ADD_TO_CART_ID_SET: {
            return {
                ...state,
                data: action.data,
                isLoading: false
            }
        }

        default:
            return state
    }
}