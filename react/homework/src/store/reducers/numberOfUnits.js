import * as NUMBER_OF_UNITS from '../constans/numberOfUnits'

const initialState = {
    data: 1,
    isLoading: true,
    error: null
}

export default function GoodsInCart(state = initialState, action) {
    switch (action.type) {
        case NUMBER_OF_UNITS.SET_NUMBER_OF_UNITS: {
            return {
                ...state,
                data: action.data,
                isLoading: false
            }
        }

        default:
            return state
    }
}