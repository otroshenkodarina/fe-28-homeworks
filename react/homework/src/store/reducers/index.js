import {combineReducers} from "redux";

import goods from './goods';
import goodsInCart from './goodsInCart'
import selectedGoods from './selectedGoods'
import quantity from './quantity'
import addToCartId from './addToCartId'
import isActiveModal from './isActiveModal'
import numberOfUnits from './numberOfUnits'


export default combineReducers({
    goods,
    goodsInCart,
    selectedGoods,
    quantity,
    addToCartId,
    isActiveModal,
    numberOfUnits
});

