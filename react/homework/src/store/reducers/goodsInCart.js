import * as GOODS_IN_CART from '../constans/goodsInCart'

const initialState = {
    data: JSON.parse(localStorage.getItem('goodsInCart')) || [],
    isLoading: true,
    error: null
}

export default function GoodsInCart(state = initialState, action) {
    switch (action.type) {
        case GOODS_IN_CART.GOODS_IN_CART_SET: {
            return {
                ...state,
                data: action.data,
                isLoading: false
            }
        }

        default:
            return state
    }
}