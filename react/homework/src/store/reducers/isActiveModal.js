import * as ACTIVE_MODAL from '../constans/isActiveModal';

const initialState = {
    data: false,
    isLoading: true,
    error: null
}

export default function IsActiveModal(state = initialState, action) {
    switch (action.type) {
        case ACTIVE_MODAL.SET_ACTIVE_MODAL: {
            return {
                ...state,
                data: action.data,
                isLoading: false
            }
        }

        default:
            return state
    }
}
