import React, {useEffect} from 'react';
import './Cart.scss'
import {useDispatch, useSelector} from "react-redux";
import * as cartActions from '../../store/actions/goodsInCart'
import * as quantityActions from "../../store/actions/quantity";
import Goods from "../../components/Goods/Goods";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";
import * as addToCartIdActions from "../../store/actions/addToCartId";
import * as goodsActions from "../../store/actions/goods";
import * as setActiveModal from '../../store/actions/isActiveModal'
import MyFormik from "../../components/MyFormik/MyFormik";


const Cart = () => {
    const dispatch = useDispatch();
    const goodsState = useSelector(state => state.goods.data)
    const cartState = useSelector(state => state.goodsInCart.data)
    const quantity = useSelector(state => state.quantity.data)
    const addToCartID = useSelector(state => state.addToCartId.data)
    const isActiveModal = useSelector(state => state.isActiveModal.data)

    useEffect(() => {
        dispatch(goodsActions.setGoods())
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    const result = goodsState.filter(item => cartState.includes(item.vendorCode));

    if (cartState?.length <= 0) {
        return (
            <div className='cart'>
                <div className='cart-empty'>
                    <Header/>
                    <p className='no-items'>No items had been added</p>
                </div>
            </div>
        )
    }

    const deleteFromCart = (itemID) => {
        const idIndex = cartState.indexOf(itemID)

        cartState.splice(idIndex, 1)
        localStorage.setItem('goodsInCart', JSON.stringify(cartState))

        dispatch(cartActions.setGoodsInCart(cartState))
        dispatch(quantityActions.setQuantity(quantity - 1))
        dispatch(setActiveModal.setIsActiveModal(false))
    }

    const handleCloseButton = (e) => {
        if (e.target.className === 'modal-container') {
            dispatch(setActiveModal.setIsActiveModal(false))
        }
    }

    return (

        <div className='cart'>
            <Header/>

            <MyFormik/>

            <div className='cart-list'>
                {result?.map((item, key) => <Goods
                    key={key}
                    name={item.name}
                    price={item.price}
                    img={item.source}
                    vendorCode={item.vendorCode}
                    color={item.color}
                    deleteFromCart={() => {
                        dispatch(addToCartIdActions.setCartId(item.vendorCode))
                        dispatch(setActiveModal.setIsActiveModal(true))
                    }}
                    closeBtn={true}
                    isShowedButtons={false}
                />)}
            </div>

            {
                isActiveModal && <Modal
                    header='Delete from cart'
                    closeModal={() => dispatch(setActiveModal.setIsActiveModal(false))}
                    handleCloseButton={handleCloseButton}
                    closeButton={true}
                    text='Do you want delete this product to cart?'
                    actions={
                        <div className='modal__buttons'>
                            <Button
                                text='Yes, sure'
                                onClick={() => deleteFromCart(addToCartID)}
                                backgroundColor='darkseagreen'
                            />
                            <Button
                                text='Cancel'
                                onClick={() => {
                                    dispatch(addToCartIdActions.setCartId(null))
                                    dispatch(setActiveModal.setIsActiveModal(false))
                                }
                                }
                                backgroundColor='darksalmon'
                            />
                        </div>
                    }
                />
            }


            <Footer/>
        </div>
    )

}

export default Cart;