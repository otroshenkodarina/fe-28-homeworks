import './MainPage.scss';
import React, {useEffect} from 'react'
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";
import GoodsList from "../../components/GoodsList/GoodsList";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import {useSelector, useDispatch} from "react-redux";
import * as goodsActions from '../../store/actions/goods';
import * as selectedActions from '../../store/actions/selectedGoods'
import * as quantityActions from "../../store/actions/quantity";
import * as addToCartIdActions from "../../store/actions/addToCartId";
import * as setActiveModal from '../../store/actions/isActiveModal'


const MainPage = () => {

    const goodsState = useSelector(state => state.goods.data)
    const cartState = useSelector(state => state.goodsInCart.data)
    const selectedState = useSelector(state => state.selectedGoods.data)
    const quantity = useSelector(state => state.quantity.data)
    const addToCartID = useSelector(state => state.addToCartId.data)
    const isActiveModal = useSelector(state => state.isActiveModal.data)

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(goodsActions.setGoods())
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    const openModal = (cartID) => {
        dispatch(addToCartIdActions.setCartId(cartID))
        dispatch(setActiveModal.setIsActiveModal(true))
    }

    const addToCart = () => {

        cartState.push(addToCartID)
        localStorage.setItem('goodsInCart', JSON.stringify(cartState))

        dispatch(quantityActions.setQuantity(quantity + 1));
        dispatch(setActiveModal.setIsActiveModal(false))
    }

    const clickStarFunc = (cartID) => {
        if (!selectedState.includes(cartID)) {
            selectedState.push(cartID)
        }
        localStorage.setItem('selectedGoods', JSON.stringify(selectedState))
        dispatch(selectedActions.setSelectedGoods(selectedState))
    }

    const handleCloseButton = (e) => {
        if (e.target.className === 'modal-container') {
            dispatch(setActiveModal.setIsActiveModal(false))
        }
    }

    return (
        <div className='App'>
            <Header/>
            <GoodsList
                goods={goodsState}
                onClick={openModal}
                clickStarFunc={clickStarFunc}
            />
            {
                isActiveModal
                && <Modal
                    closeModal={() => dispatch(setActiveModal.setIsActiveModal(false))}
                    handleCloseButton={handleCloseButton}
                    header='PRODUCT CART'
                    closeButton={true}
                    text='Do you want to add this product to cart?'
                    actions={
                        <div className='modal__buttons'>
                            <Button
                                text='Yes, sure'
                                onClick={addToCart}
                                backgroundColor='darkseagreen'
                            />
                            <Button
                                text='Cancel'
                                onClick={() => {
                                    dispatch(addToCartIdActions.setCartId(null))
                                    dispatch(setActiveModal.setIsActiveModal(false))
                                }
                                }
                                backgroundColor='darksalmon'
                            />
                        </div>
                    }
                />
            }

            <Footer/>
        </div>
    )
};

export default MainPage;