const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function makeList(array, parent) {
    const list = document.createElement('ul')
    document.querySelector(parent).append(list)

    array.forEach((item) => {
        try {
            if (!item.hasOwnProperty('author')) {
                throw new Error('не хватает свойства author')
            } else if (!item.hasOwnProperty('name')) {
                throw new Error('не хватает свойства name')
            } else if (!item.hasOwnProperty('price')) {
                throw new Error('не хватает свойства price')
            } else {
                const listItem = document.createElement('li')
                listItem.innerText = `${item.author} "${item.name}" - ${item.price}$`
                list.append(listItem)
            }
        } catch (e) {
            console.error(e)
        }
    })
}


makeList(books, '#root')