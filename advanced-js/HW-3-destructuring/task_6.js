const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const {name, surname, age = 49, salary = 'скрыто'} = employee

const employee2 = {
    name,
    surname,
    age,
    salary
}

console.log(employee2);