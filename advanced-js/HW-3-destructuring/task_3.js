const user1 = {
    name: "John",
    years: 30
};

const {name: name, years: age, isAdmin: isAdmin = false} = user1

const user2 = {
    name,
    age,
    isAdmin
}

console.log(user2);

const text = document.createElement('p')
text.innerText = `name: ${name}, age: ${age}, is admin: ${isAdmin}`

document.querySelector("body").prepend(text)
