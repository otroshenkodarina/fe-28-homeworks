const button = document.querySelector('.btn')
let counter = 0

button.addEventListener('click', async function (e) {
    counter ++
    if (counter === 1){
    const ip = await fetch('https://api.ipify.org/?format=json').then(r => r.json())
    const address = await fetch(`http://ip-api.com/json/${ip.ip}`).then(r => r.json())
        console.log(address);
        createAddress(address)}
else {
        return
    }
})

function createAddress (address){
    const addressEl = document.createElement('p')
    addressEl.classList.add('address')
    addressEl.innerText = `Континент: ${address.timezone}
    Страна: ${address.country} 
    Регион: ${address.regionName}
    Город: ${address.city}`

    document.querySelector('.parent').append(addressEl)
}