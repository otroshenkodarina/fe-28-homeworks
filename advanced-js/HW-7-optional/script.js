// class user
// class post
// class card

document.addEventListener('DOMContentLoaded', async function (e) {
    const userListRes = await fetch('https://ajax.test-danit.com/api/json/users').then(r => r.json());
    const userList = userListRes.map(item => new User(item));

    const postListRes = await fetch('https://ajax.test-danit.com/api/json/posts').then(r => r.json());
    const postList = postListRes.map(item => new Post({
        ...item,
        user: userList.find(user => user.getUserID() === item.userId)
    }));


});

class User {
    #id;

    constructor({id, name, email}) {
        this.#id = id;
        this.name = name;
        this.email = email;
    }

    getUserID() {
        return this.#id;
    }
}

class Post {
    constructor({id, user, title, body}) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.body = body;
    }
}


class Card {
    constructor() {
        this.elements = {
            // cardBox: document.createElement('div'),
            // cardTitle: document.createElement('h2'),
            // cardText: document.createElement('p'),
            // userName: document.createElement('p'),
            // userEmail: document.createElement('a'),
            // button: document.createElement('button'),
        }
    }

    render() {
        document.addEventListener('DOMContentLoaded', async function (e) {
            const users = await fetch('https://ajax.test-danit.com/api/json/users').then(r => r.json())
            const posts = await fetch('https://ajax.test-danit.com/api/json/posts').then(r => r.json())

            users.forEach((user) => {
                posts.forEach((post) => {
                    if (user.id === post.userId) {
                        const cardBox = document.createElement('div')
                        cardBox.classList.add('card')

                        const button = document.createElement('button')
                        button.classList.add('button')
                        button.innerText = 'DELETE POST'

                        const cardTitle = document.createElement('h2')
                        cardTitle.classList.add('title')
                        cardTitle.innerText = `${post.title}`

                        const cardText = document.createElement('p')
                        cardText.classList.add('text')
                        cardText.innerText = `${post.body}`

                        const userName = document.createElement('p')
                        userName.classList.add('user-name')
                        userName.innerText = `${user.name}`

                        const userEmail = document.createElement('a')
                        userEmail.href = '#'
                        userEmail.classList.add('user-email')
                        userEmail.innerText = `${user.email}`


                        button.addEventListener('click', async function (e) {
                            const request = await fetch('https://ajax.test-danit.com/api/json/posts/${postId}')
                            if (request) {
                                cardBox.remove()
                            }
                        })

                        document.querySelector('body').append(cardBox)
                        cardBox.append(userName, userEmail, cardTitle, cardText, button)
                    }
                })
            })
        })


    }
}

const card = new Card()
card.render()













