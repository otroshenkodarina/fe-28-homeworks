class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name
    }

    get age() {
        return this._age
    }


    get salary() {
        return this._salary
    }

    set name(value) {
        if (value.length < 4) {
            alert("Имя слишком короткое.");
            return;
        }
        this._name = value;
    }

    set age(value) {
        if (value < 18) {
            alert("Работник несовершеннолетний!");
            return;
        }
        this._age = value;
    }

    set salary(value) {
        if (value < 50000) {
            alert("Кто будет работать за такие деньги?");
            return;
        }
        this._salary = value;
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }
    get salary() {
        return super.salary * 3
    }

    set salary(value) {
        if (value < 50000) {
            alert("Кто будет работать за такие деньги?");
            return;
        }
        super.salary = value * 3;
    }
}


const programmer1 = new Programmer('Fedia', 23, 70000, 'en, ua, ru')
const programmer2 = new Programmer('Vasia', 20, 80000, 'en, ua, ru')
const programmer3 = new Programmer('Vania', 22, 60000, 'en, ua, ru')
const programmer4 = new Programmer('Andrew',25, 80000, 'en, ua, ru')
const programmer5 = new Programmer('Dashenka', 20, 100000, 'en, ua, ru')

console.log(programmer1, programmer2, programmer3, programmer4, programmer5);
